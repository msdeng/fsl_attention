import numpy as np   
import pandas as pd
import random
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import numpy as np
from rdkit import Chem
from rdkit.Chem import Draw

import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import numpy as np
from rdkit import Chem
from rdkit.Chem import Draw

class Adjacency:
    """Adjacency class for MolGraph"""
    def __init__(self, mol):
        """initializing the num of atoms in mol and the mol's adjacency matrix"""
        self.num_atoms = mol.GetNumAtoms()
        self.adj_mat = self._get_adjacency_matrix(mol)
    
    def _get_adjacency_matrix(self, mol):
        adj_mat = np.zeros((self.num_atoms, self.num_atoms))
        for bond in mol.GetBonds():
            begin_idx = bond.GetBeginAtomIdx()
            end_idx = bond.GetEndAtomIdx()
            adj_mat[begin_idx][end_idx] = 1
            adj_mat[end_idx][begin_idx] = 1
        return adj_mat
    
    @property
    def id_adj_mat(self):
        """identity + adjacency matrix"""
        return self.adj_mat + np.eye(self.num_atoms)
    

class Nodes:
    """node class for mol graph"""
    def __init__(self, mol):
        """initializing node feature"""
        self.node_feat = self._get_node_features(mol)
        
    def _get_node_features(self, mol):
        node_feat = [self._featurize_atom(atom) for atom in mol.GetAtoms()]
        node_feat = np.array(node_feat)
        return node_feat
    def _featurize_atom(self, atom):
        
        results = (Nodes.one_of_k_encoding(atom.GetSymbol(), ## one-hot of atom symbol
                                           [
                                                "C",
                                                "N",
                                                "O",
                                                "S",
                                                "F",
                                                "Si",
                                                "P",
                                                "Cl",
                                                "Br",
                                                "Mg",
                                                "Na",
                                                "Ca",
                                                "Fe",
                                                "As",
                                                "Al",
                                                "I",
                                                "B",
                                                "V",
                                                "K",
                                                "Tl",
                                                "Yb",
                                                "Sb",
                                                "Sn",
                                                "Ag",
                                                "Pd",
                                                "Co",
                                                "Se",
                                                "Ti",
                                                "Zn",
                                                "H",  # H?
                                                "Li",
                                                "Ge",
                                                "Cu",
                                                "Au",
                                                "Ni",
                                                "Cd",
                                                "In",
                                                "Mn",
                                                "Zr",
                                                "Cr",
                                                "Pt",
                                                "Hg",
                                                "Pb",
                                                "Unknown",
                                            ], unk=True)
                  + Nodes.one_of_k_encoding(atom.GetDegree(), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
                  + Nodes.one_of_k_encoding(atom.GetNumImplicitHs(), [0, 1, 2, 3, 4, 5, 6], unk=True)
                  + [atom.GetFormalCharge()]
                  + Nodes.one_of_k_encoding(atom.GetHybridization(), [
                      Chem.rdchem.HybridizationType.S,
                      Chem.rdchem.HybridizationType.SP, 
                      Chem.rdchem.HybridizationType.SP2,
                      Chem.rdchem.HybridizationType.SP3, 
                      Chem.rdchem.HybridizationType.SP3D,
                      Chem.rdchem.HybridizationType.SP3D2,],unk=True)
                  + [atom.GetIsAromatic()]
                  + Nodes.one_of_k_encoding(atom.GetNumExplicitHs(), [0, 1, 2, 3, 4], unk=True))
        return np.array(results)
    
    def one_of_k_encoding(x, allowable_set, unk=False):
        if x not in allowable_set:
            if unk:
                x = allowable_set[-1]
            else:
                print(x)
                raise Exception("input {0} not in allowable set{1}:".format(x, allowable_set))
        return list(map(lambda s: x == s, allowable_set))
    
    
class MolGraph(Adjacency, Nodes):
    """mol graph corresponding a specified molecule"""
    def __init__(self, smiles, explicit_H=None):
        """construct a mol graph from smiles"""
        self.mol = Chem.MolFromSmiles(smiles)
        #self.mol = Chem.MolFromSmiles(smiles, sanitize=False)
        Adjacency.__init__(self, self.mol)
        Nodes.__init__(self, self.mol)

class GraphData(Dataset):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.dfeat = torch.tensor([len(x[0][1][0])]).float()
        self.scale = 1 / self.dfeat * 10
        
    def __len__(self):
        return len(self.x)
    
    def __getitem__(self, idx):
        x = self.x[idx]
        adj, node_feat = torch.FloatTensor(x[0]), torch.FloatTensor(x[1])
        adj = adj.long()
        adj = F.pad(adj, (0, 1, 0, 0), "constant", 0).float()
        adj = F.pad(adj, (0, 0, 0, 1), "constant", 1).float()
        #print(adj)
        """邻接矩阵归一化： D^-1AD^-1"""
        """rowsum = adj.sum(1)           #每行的数加在一起
        r_inv_sqrt = torch.pow(rowsum, -0.5).flatten()   #输出rowsum ** -1/2
        r_inv_sqrt[torch.isinf(r_inv_sqrt)] = 0.         #溢出部分赋值为0
        r_mat_inv_sqrt = torch.diag(r_inv_sqrt)         #对角化
        adj = torch.matmul(adj, r_mat_inv_sqrt)
        adj = torch.transpose(adj, 0, 1)                   #转置
        adj = torch.matmul(adj, r_mat_inv_sqrt)"""
        
        
        """ 节点级特征归一化 """
        """for node in node_feat:
            print(node)"""

        #node_feat = torch.mul(node_feat, torch.ones(len(node_feat), 1) * self.scale)
        #node_feat = torch.cat((node_feat.float(), node_feat.float().mean(0).view(1, -1)))
        node_feat = F.pad(node_feat, (0, 0, 0, 1), "constant", 0)
        x = [adj, node_feat]
        a = torch.ones(len(x[1]), 1)
        x.append(a)
        y = torch.tensor(self.y[idx])
        if len(y.size()) < 1:
            y = y.unsqueeze(-1)
        return x, y

import torch.nn.functional as F
def post_batch_padding_collate_fn(batch_data):
    #start = time.time()
    x, y = zip(*batch_data)
    #print(y)
    adj_mat, node_feat, atom_vec= zip(*x)
    num_atoms = [len(v) for v in atom_vec]
    padding_final = np.max(num_atoms)
    #print(padding_final)
    #padding_final = np.array([301])
    padding_len = padding_final - num_atoms
    #test = [F.pad(a, (0, p, 0, p), "constant", 0) for a, p in zip(adj_mat, padding_len)]
    adj_mat = torch.stack([F.pad(a, (0, p, 0, p), "constant", 0) for a, p in zip(adj_mat, padding_len)], 0)
    node_feat = torch.stack(
        [F.pad(n, (0, 0, 0, p), "constant", 0) for n, p in zip(node_feat, padding_len)], 0
    )
    atom_vec = torch.stack(
        [F.pad(v, (0, 0, 0, p), "constant", 0) for v, p in zip(atom_vec, padding_len)], 0
    )
    x = [adj_mat, node_feat, atom_vec]
    #x = [adj_mat, node_feat]
    y = torch.stack(y, 0)
    #end = time.time()
    #print(end - start)
    return x, y

class TrainSampler():
    def __init__(self, labels, pos_label=1, inner_batch_size=20, n_query=128, iterations=2):
        """
        batch size = num pos + num neg
        """
        super(TrainSampler, self).__init__()
        self.labels = labels
        self.n_query = n_query
        self.idx = np.array(list(range(len(self.labels))))
        self.pos_label = pos_label
        assert inner_batch_size % 2 == 0
        self.num_pos = int(inner_batch_size / 2)
        self.num_neg = int(inner_batch_size / 2)
        self.iterations = iterations
        self.batch_size = inner_batch_size
        self.pos_idx = np.where(self.labels == self.pos_label)[0]
        self.neg_idx = np.where(self.labels != self.pos_label)[0]
    
    def __iter__(self):
        for it in range(self.iterations):
            ##print(self.labels.shape)
            support = list(np.random.choice(self.idx[self.pos_idx].reshape(-1), self.num_pos)) +\
                      list(np.random.choice(self.idx[self.neg_idx].reshape(-1), self.num_neg))
            query = list(np.random.choice(self.idx.reshape(-1), self.n_query)) 
            random.shuffle(support)
            random.shuffle(query)
            batch = support + query
            yield batch
            
    def __len__(self):
        return self.iterations

class TestSampler():
    def __init__(self, labels, pos_label=1, inner_batch_size=20, n_query=128, iterations=2):
        """
        batch size = num pos + num neg
        """
        super(TestSampler, self).__init__()
        self.labels = labels
        self.n_query = n_query
        self.idx = np.array(list(range(len(self.labels))))
        self.pos_label = pos_label
        assert inner_batch_size % 2 == 0
        self.num_pos = int(inner_batch_size / 2)
        self.num_neg = int(inner_batch_size / 2)
        self.iterations = iterations
        self.batch_size = inner_batch_size
        self.pos_idx = np.where(self.labels == self.pos_label)[0]
        self.neg_idx = np.where(self.labels != self.pos_label)[0]
    
    def __iter__(self):
        for it in range(self.iterations):
            ##print(self.labels.shape)
            support = list(np.random.choice(self.idx[self.pos_idx].reshape(-1), self.num_pos)) +\
                      list(np.random.choice(self.idx[self.neg_idx].reshape(-1), self.num_neg))
            query_idx = []
            for idx in self.idx:
                if idx not in query_idx:
                    query_idx.append(idx)
            query = query_idx
            random.shuffle(support)
            random.shuffle(query)
            batch = support + query
            yield batch
            
    def __len__(self):
        return self.iterations

import numpy as np   
import pandas as pd
import random
tox21_path = "../dataset/tox21.csv"
df_tox21 = pd.read_csv(tox21_path)
all_smiles_tox21 = df_tox21['smiles'].values
data_mol_graph = []
for smiles in all_smiles_tox21:
    mol_graph = MolGraph(smiles)
    adj, node_feat = mol_graph.id_adj_mat, mol_graph.node_feat
    data_mol_graph.append([adj, node_feat])

data_mol_graph = np.array(data_mol_graph, dtype=object)

task_name = ['NR-AR', 'NR-AR-LBD', 'NR-AhR', 'NR-Aromatase', 'NR-ER', 'NR-ER-LBD','NR-PPAR-gamma', 'SR-ARE', 'SR-ATAD5', 'SR-HSE', 'SR-MMP', 'SR-p53']

train_task_name = task_name[:-3]
test_task_name = task_name[-3:]

train_data = []
for name in train_task_name:
    r = df_tox21[name].values
    assert any(np.isnan(r))
    if any(np.isnan(r)):
        mask = ~np.isnan(r)
    x = data_mol_graph[mask]
    y = r[mask]
    task_data = GraphData(x, y)
    sampler = TrainSampler(labels=y)
        #print(y)
    data_loader = DataLoader(dataset=task_data,
                            collate_fn=post_batch_padding_collate_fn,
                            batch_sampler=sampler)
    
    train_data.append(data_loader) 

test_data = []
for name in test_task_name:
    r = df_tox21[name].values
    assert any(np.isnan(r))
    if any(np.isnan(r)):
        mask = ~np.isnan(r)
    x = data_mol_graph[mask]
    y = r[mask]
    task_data = GraphData(x, y)
    sampler = TestSampler(labels=y)
        #print(y)
    data_loader = DataLoader(dataset=task_data,
                            collate_fn=post_batch_padding_collate_fn,
                            batch_sampler=sampler)
    test_data.append(data_loader) 

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import torch
import torch.nn as nn

class GraphConvLayer9(nn.Module):
    """
    all node with difference degree share the same proj matrix.
    all feature activated by LeakyReLU activation function.
    suport batch data.
    using residual connect input and output
    
    """
    def __init__(self, dinput, doutput, bias=True):
        super(GraphConvLayer9, self).__init__()
        self.bias = bias
        self.w = nn.Parameter(torch.Tensor(1, dinput, doutput))
        if self.bias:
            self.b = nn.Parameter(torch.Tensor(1, 1, doutput))
        self.relu = nn.LeakyReLU()
        self._reset_parameters()
        
    
    def _reset_parameters(self):
        nn.init.xavier_normal_(self.w)
        if self.bias:
            nn.init.zeros_(self.b)
            
    def forward(self, adj, feat, atom_vec):
        if len(adj.size()) == 3:
            adj = adj.unsqueeze(1)
        output = feat.unsqueeze(1)
        
        #output = torch.matmul(adj, feat)
        output = torch.matmul(output, self.w)
        if self.bias:
            output = output + self.b
        output = torch.matmul(adj, output)
        output = output.sum(dim=1)
        output = self.relu(output)
        
        return output

class GraphConv9(nn.Module):
    """
    multi conv layer
    """
    def __init__(self, dinput=75, dhidden=75, doutput=75, bias=True):
        super(GraphConv9, self).__init__()
        self.conv1 = GraphConvLayer9(dinput, dhidden, bias=True)
        self.conv2 = GraphConvLayer9(dhidden, dhidden, bias=True)
        self.conv3 = GraphConvLayer9(dhidden, doutput, bias=True)
        #self.conv4 = GraphConvLayer9(doutput, doutput, bias=True)
        """self.conv5 = GraphConvLayer9(doutput, doutput, bias=True)
        self.conv6 = GraphConvLayer9(doutput, doutput, bias=True)
        self.conv7 = GraphConvLayer9(doutput, doutput, bias=True)
        self.conv8 = GraphConvLayer9(doutput, doutput, bias=True)
        self.conv9 = GraphConvLayer9(doutput, doutput, bias=True)
        self.conv10 = GraphConvLayer9(doutput, doutput, bias=True)"""
        self.fnn = nn.Sequential(nn.Linear(doutput, doutput),
                                 nn.Tanh())
        
    def forward(self, x):
        adj, node_feat, atom_vec = x
        
        node_feat = self.conv1(adj, node_feat, atom_vec)
        #print(node_feat[-1])
        node_feat = self.conv2(adj, node_feat, atom_vec)
        node_feat = self.conv3(adj, node_feat, atom_vec)

        output = torch.mul(node_feat, atom_vec)
        r = []
        for i in range(len(output)):
            idx = int(atom_vec[i].sum())
            r.append(output[i][idx - 1])
        output = torch.stack([_ for _ in r], dim=0)
        #print('extract feature shape: ', output.shape)
        output = self.fnn(output)
        return output
        

class AttentionLayer(nn.Module):
    def __init__(self, dmodel):
        super(AttentionLayer, self).__init__()
        self.scale = torch.sqrt(torch.tensor(dmodel).float())
        self.similarity = nn.CosineSimilarity(dim=1)
        self.mlp = nn.Sequential(nn.Linear(dmodel, dmodel),
                                  nn.LeakyReLU())
    
    def forward(self, Q, K, V):
        seq_len, d = Q.shape
        att_score = torch.matmul(Q, K.t())
        att_score = att_score / self.scale
        att_score = F.softmax(att_score, dim=1)
        output = torch.matmul(att_score, V)
        output = self.mlp(output)
        return output
    
class AttentionBlock(nn.Module):
    def __init__(self, dmodel, n_support, K):
        super(AttentionBlock, self).__init__()
        self.attn1 = AttentionLayer(dmodel)
        self.mlp1 = nn.Sequential(nn.Linear(dmodel * 2, dmodel),
                                  nn.LeakyReLU())
        self.n_support = n_support
        self.K = K

    def forward(self, x):
        x_support = x[:self.n_support]
        x_query = x[self.n_support:]
        output = self.attn1(x, x_support, x_support)
        output = self.mlp1(torch.cat((x, output), dim=1))
        output = x + output
        r = output
        
        return output
        

class MatchNet(nn.Module):
    def __init__(self, dinput, dhidden, doutput, n_support=20, K=1):
        super(MatchNet, self).__init__()
        self.extractor = GraphConv9(dinput=dinput, dhidden=dhidden, doutput=doutput)
        self.att = AttentionBlock(doutput, n_support, K)
        self.n_support = n_support
        self.similarity = nn.CosineSimilarity(dim=1)
        weight=torch.tensor([1., 10])
        self.loss_fn = nn.NLLLoss()
    
    def cos_similar(self, p, q):
        sim_matrix = p.matmul(q.transpose(-2, -1))
        a = torch.norm(p, p=2, dim=-1)
        b = torch.norm(q, p=2, dim=-1)
        sim_matrix /= a.unsqueeze(-1)
        sim_matrix /= b.unsqueeze(-2)
        return sim_matrix
    
    def get_one_hot(self, label, N):
        size = list(label.size())
        label = label.view(-1)
        ones = torch.sparse.torch.eye(N)
        ones = ones.index_select(0, label)
        size.append(N)
        return ones.view(*size)
    
    def forward(self, x, target):
        classes = torch.unique(target)
        n_classes = len(classes)
        target = target.view(-1)
        output = self.extractor(x)
        output = self.att(output)
        x_support = output[:self.n_support]
        x_query = output[self.n_support:]
        y_support = target[:self.n_support]
        y_query = target[self.n_support:]
        one_hot_y = self.get_one_hot(y_support.long(), n_classes)
        f, g = x_query, x_support
        scores = self.cos_similar(f, g)
        scores = F.softmax(scores, dim=1)
 
        p = torch.matmul(scores.cpu(), one_hot_y)
        
        loss_val = self.loss_fn(p, y_query.long())
        _, y_hat = p.max(dim=1)
        
        acc_val = y_hat.eq(y_query).float().mean()
        return loss_val,  acc_val, y_query, p[:, 1]

def to_device(x, device):
    if isinstance(x, torch.Tensor):
        return x.to(device)
    if isinstance(x, np.ndarray):
        return torch.tensor(x, device=device)
    if isinstance(x, (list, tuple)):
        x = [to_device(x_, device) for x_ in x]
        return x

import time
def train(model, optimizer, train_dataloaders, val_dataloaders, device, iterations=10):
    train_loss = []
    train_acc = []
    start = time.time()
    for iteration in range(1, iterations +1):
        model.train()
        optimizer.zero_grad()
        random_task_indices = np.random.choice(
                    list(range(len(train_dataloaders))),
                    size=1,
                    replace=True)
        task_data = iter(train_dataloaders[random_task_indices[0]])
        x, y = task_data.next()
        x = to_device(x, device)
        loss, acc, y_query, score = model(x, y)
        #print(prototypes)
        #print(output.size())
        train_acc.append(acc)
        train_loss.append(loss.item())
        loss.backward()
        del x, y
        """named_param = list(enumerate(model.named_parameters()))
        print(named_param[0][1][1])
        print('grad', named_param[0][1][1].grad)"""
        optimizer.step()
        if iteration % 500 == 0:
            print(f'{iteration}-th iteration')
            print(f'avg train loss: {np.mean(train_loss[-500:])}')
            print(f'avg train acc:{np.mean(train_acc[-500:])}')
    end = time.time()
    print('total train time:', end - start)

import pickle
import numpy as np
from sklearn.metrics import roc_curve, auc
import random
import learn2learn as l2l
import os.path as path
import time
from torch import optim

seed = 323
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
device = torch.device("cuda:1" if torch.cuda.is_available() else "cpu")
    
if torch.cuda.device_count() > 1:
    print("Let's use", torch.cuda.device_count(), "GPUs!")
model = MatchNet(dinput=75, dhidden=128, doutput=64, n_support=20, K=1)
model = model.to(device)
#model = FNN(fc_dims=[128, 256, 512, 1024, 1024, 1])
print(next(model.parameters()).is_cuda)
lr = 1e-4
iterations = 20000
#loss_fn = nn.BCELoss()
optimizer = optim.Adam(model.parameters(),lr=lr)
#train(model, optimizer, train_dataloaders, None, device=device, iterations=iterations)
train(model, optimizer, train_data, None, device=device, iterations=iterations)

def test(model, val_dataloaders, loss_fn, device, iterations=10):
    r_auroc = 0
    r_acc = 0
    for i in range(0, len(val_dataloaders)):
        print(f'test task {i + 1}')
        val_loss = []
        val_acc = []
        auroc = []
        for iteration in range(1, iterations +1):
            with torch.no_grad():
                task_data = iter(val_dataloaders[i])
                x, y, = task_data.next()
                x = to_device(x, device)
                loss, acc, y_query, score = model(x, y)
         
                fpr, tpr, thersholds = roc_curve(y_query, score, pos_label=1)
                roc_auc = auc(fpr, tpr)
                auroc.append(roc_auc)
                val_acc.append(acc)
                del x, y
        #print(f'avg test acc:{np.mean(val_acc)}')
        print(f'avg test auroc: ',format(np.mean(auroc), '.4f'))
        #print(f'avg test auroc:{np.mean(auroc)}')
        r_auroc += np.mean(auroc)
        r_acc += np.mean(val_acc)
    #print(f'mean acc: ',format(r_acc / len(val_dataloaders), '.4f'))
    print(f'mean auroc: {r_auroc / len(val_dataloaders)}')
test(model, test_data, None, device, iterations=20)