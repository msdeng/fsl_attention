# Molecular Property Prediction by Few-Shot Learning Based on Attention Mechanism

By Yuan Liu, Hujun Shen, Changrui Guo，Jianping Chen, Shaohong Cai and Mingsen Deng 


## Usage
python AE_FSL.py

## Support
deng (at) gznc.edu.cn

## License
LGPL-3.0 License
